#!/usr/bin/env python3
import sys


progress_last_len = 0
def log(msg):
    global progress_last_len
    if progress_last_len > 0:
        sys.stderr.write('\n')
        progress_last_len = 0
    sys.stderr.write(msg + '\n');
    sys.stderr.flush()

def progress(msg):
    global progress_last_len
    if progress_last_len > 0:
        sys.stderr.write(' ' * progress_last_len + '\r')
    sys.stderr.write(msg + '\r')
    sys.stderr.flush()
    progress_last_len = len(msg)

class Solver(object):
    def __init__(self, R, C, L, H, pizza):
        self.R = R
        self.C = C
        self.L = L
        self.H = H
        self.pizza = pizza
        self.slicing = [[None] * C for x in range(R)]
        self.possible_slices = self.compute_possible_slices()

    def compute_possible_slices(self):
        possible_slices = []
        for i in range(1, self.H+1):
            for j in range(1, self.H//i + 1):
                if i * j < 2 * self.L:
                    continue
                possible_slice = (i, j)
                possible_slices.append(possible_slice)

        def size(s):
            return s[0] * s[1]

        return sorted(possible_slices, key=size, reverse=True)

    def check_slice(self, r, c, possible_slice, with_slicing=True):
        r_slice, c_slice = possible_slice
        mushrooms, tomatoes = 0, 0
        for i in range(r_slice):
            for j in range(c_slice):
                if r+i >= self.R or c+j >= self.C:
                    return False
                if with_slicing and self.slicing[r+i][c+j] is not None:
                    return False
                if self.pizza[r+i][c+j] == 0:
                    tomatoes += 1
                elif self.pizza[r+i][c+j] == 1:
                    mushrooms += 1
                else:
                    raise Exception('Error')
        return tomatoes >= self.L and mushrooms >= self.L

    def cut_slice(self, r, c, slice):
        r_slice, c_slice = slice
        cells = []
        for i in range(r_slice):
            for j in range(c_slice):
                self.slicing[r+i][c+j] = (r, c, slice)
                cells.append((r+i, c+j))
        return cells

    def paste_slice(self, r, c, slice):
        r_slice, c_slice = slice
        cells = []
        for i in range(r_slice):
            for j in range(c_slice):
                self.slicing[r+i][c+j] = None
                cells.append((r+i, c+j))
        return cells

    def solve(self):
        slices = []
        score = 0
        log('Solving')
        for r in range(self.R):
            progress('Progress: %.1f %% - Score %d' % (r / self.R * 100, score))
            for c in range(self.C):
                if self.slicing[r][c] is not None:
                    continue
                for possible_slice in self.possible_slices:
                    if self.check_slice(r, c, possible_slice):
                        self.cut_slice(r, c, possible_slice)
                        score += possible_slice[0] * possible_slice[1]
                        slices.append((r, c, r+possible_slice[0]-1, c+possible_slice[1]-1))
                        break
        log('Solving done - Score: %d' % score)

        # optimizer
        log('Optimmizing')
        old_score = score
        slices = set(slices)
        cells = []
        for r in range(R):
            for c in range(C):
                if self.slicing[r][c] is not None:
                    continue
                cells.append((r, c))

        step = 0
        step_score = score
        while cells:
            step += 1
            count = len(cells)
            future_cells = []
            log('Step %d - %d uncut cells' % (step, count))
            for i, (r, c) in enumerate(cells):
                if i % max(10, count // 1000) == 0:
                    progress('Progress: %.1f %% - Score %d' % (i / count * 100, score))
                if self.slicing[r][c] is not None:
                    continue
                score_diff, removed, added, new_cells = self.optimize_at(r, c)
                future_cells.extend(new_cells)
                if score_diff > 0:
                    score += score_diff
                    for r, c, (h, w) in removed:
                        slices.remove((r, c, r+h-1, c+w-1))
                    for r, c, (h, w) in added:
                        slices.add((r, c, r+h-1, c+w-1))
            log('Step %d - Score: %d - Improvement: %d' % (step, score, score - step_score))
            step_score = score
            cells = future_cells
        log('Optimization done - Score: %d - Total improvement: %d' % (score, score - old_score))
        return score, list(slices)

    def optimize_at(self, r, c):
#        log('Hole at (%d, %d)' % (r, c))
        potential_slices = self.get_possible_slices_at(r, c)
        max_score = 0
        best_slice = None
        best_conflicts = None
        best_replacements = None
        for slice in potential_slices:
#            log('  Slice: %r' % (slice,))
            conflicting_cells, conflicts = self.get_slice_conflicts(slice)
#            log('    Conflicts: %r' % conflicts)
            score = slice[2][0] * slice[2][1]
            replacements = []
            for conflict in conflicts:
                value, new_slices = self.resolve_conflict(slice, conflict)
                score += value
                replacements.extend(new_slices)
            if score > max_score:
                max_score = score
                best_slice = slice
                best_conflicts = conflicts
                best_replacements = replacements
#        log('  Best: %d --> %r --> %r' % (max_score, best_slice, best_replacements))
        if max_score > 0:
            new_cells = set()
            for slice in best_conflicts:
                new_cells.update(self.paste_slice(*slice))
            added = best_replacements
            added.append(best_slice)
            for slice in added:
                new_cells.difference_update(self.cut_slice(*slice))
            return max_score, best_conflicts, added, list(new_cells)
        else:
            return 0, [], [], []

    def get_slice_conflicts(self, slice):
        conflicts = set()
        count = 0
        (r, c, (r_size, c_size)) = slice
        for i in range(r_size):
            for j in range(c_size):
                if self.slicing[r+i][c+j] is not None:
                    count += 1
                    conflicts.add(self.slicing[r+i][c+j])
        return count, conflicts

    def resolve_conflict(self, slice, conflict):
        (sr, sc, (sh, sw)) = slice
        (cr, cc, (ch, cw)) = conflict
        c_size = ch * cw
        v_slices = []
        v_count = 0
        if sr > cr:
            if self.check_slice(cr, cc, (sr - cr, cw), with_slicing=False):
                v_slices.append((cr, cc, (sr - cr, cw)))
                v_count += (sr - cr) * cw
        if sr + sh < cr + ch:
            if self.check_slice(sr + sh, cc, (cr + ch - (sr + sh), cw), with_slicing=False):
                v_slices.append((sr + sh, cc, (cr + ch - (sr + sh), cw)))
                v_count += (cr + ch - (sr + sh)) * cw

        h_slices = []
        h_count = 0
        if sc > cc:
            if self.check_slice(cr, cc, (ch, sc - cc), with_slicing=False):
                h_slices.append((cr, cc, (ch, sc - cc)))
                h_count += ch * (sc - cc)
        if sc + sw < cc + cw:
            if self.check_slice(cr, sc + sw, (ch , cc + cw - (sc + sw)), with_slicing=False):
                h_slices.append((cr, sc + sw, (ch , cc + cw - (sc + sw))))
                h_count += ch * (cc + cw - (sc + sw))

        if v_count > h_count:
            return v_count - c_size, v_slices
        else:
            return h_count - c_size, h_slices

    def get_possible_slices_at(self, r, c):
        slices = []
        for (r_slice, c_slice) in self.possible_slices:
            for i in range(r_slice):
                for j in range(c_slice):
                    if r-i < 0 or c-j < 0:
                        continue
                    if self.check_slice(r-i, c-j, (r_slice, c_slice), with_slicing=False):
                        slices.append((r - i, c - j, (r_slice, c_slice)))
        return slices

if __name__ == '__main__':
    # Input parsing
    R, C, L, H = map(int, input().split(' '))
    pizza = []
    for i in range(R):
        pizza.append(list(map(lambda x: 1 if x == 'M' else 0, input())))

    # Computing
    solver = Solver(R, C, L, H, pizza)
    score, slices = solver.solve()

    # Output
    log('Score: %d' % score)
    print(len(slices))
    for slice in slices:
        print(' '.join(map(str, slice)))
