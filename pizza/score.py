#!/usr/bin/env python3
import sys

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('Missing filenames')
        sys.exit(-1)

    f = open(sys.argv[1])
    R, C, L, H = [int(x) for x in f.readline().split()]
    pizza = []
    for i in range(R):
        pizza.append(list(f.readline().strip()))

    f = open(sys.argv[2])
    S = int(f.readline())
    slices = []
    for i in range(S):
        slices.append(tuple([int(x) for x in f.readline().split()]))

    score = 0
    matrix = [[False] * C for x in range(0, R)]
    for slice in slices:
        r1, c1, r2, c2 = slice
        size = (1 + abs(r1 - r2)) * (1 + abs(c1 - c2))
        if size > H:
            print('Error: Slice %r is too big' % slice)
            sys.exit(1)
        mushrooms = 0
        tomatoes = 0
        for row in range(min(r1, r2), max(r1, r2) + 1):
            for col in range(min(c1, c2), max(c1, c2) + 1):
                if matrix[row][col]:
                    print('Error: Slice %r conflicts with another slice.' % slice)
                    sys.exit(1)
                matrix[row][col] = True
                if pizza[row][col] == 'T':
                    tomatoes += 1
                else:
                    mushrooms += 1
        if mushrooms < L:
            print('Error: Not enough mushrooms on slice %r' % slice)
            sys.exit(1)
        if tomatoes < L:
            print('Error: Not enough tomatoes on slice %r' % slice)
            sys.exit(1)
        score += size
    print('Score: %d' % score)
