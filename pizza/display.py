#!/usr/bin/env python3
import sys

def colorize_cell(pizza, slicing, r, c):
    color_map = {
        0: '\033[41m',
        1: '\033[42m',
        2: '\033[43m\033[30m',
        3: '\033[44m',
        4: '\033[45m',
        5: '\033[46m',
        6: '\033[47m\033[30m',
    }
    color = slicing[r][c]
    letter = pizza[r][c]
    if color is None:
        return letter
    else:
        return color_map[color] + letter + '\033[0m'

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('Missing filenames')
        sys.exit(-1)

    f = open(sys.argv[1])
    R, C, L, H = [int(x) for x in f.readline().split()]
    pizza = []
    for i in range(R):
        pizza.append(list(f.readline().strip()))

    f = open(sys.argv[2])
    S = int(f.readline())
    slices = []
    for i in range(S):
        slices.append(tuple([int(x) for x in f.readline().split()]))

    slicing = [[None] * C for x in range(R)]
    color = 0
    for r1, c1, r2, c2 in slices:
        for row in range(min(r1, r2), max(r1, r2) + 1):
            for col in range(min(c1, c2), max(c1, c2) + 1):
                slicing[row][col] = color
        color = (color + 1) % 7

    for r in range(R):
        print(''.join([colorize_cell(pizza, slicing, r, c) for c in range(C)]))
