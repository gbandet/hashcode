import sys

progress_last_len = 0
def log(msg):
    global progress_last_len
    if progress_last_len > 0:
        sys.stderr.write('\n')
        progress_last_len = 0
    sys.stderr.write(msg + '\n');
    sys.stderr.flush()

def progress(msg):
    global progress_last_len
    if progress_last_len > 0:
        sys.stderr.write(' ' * progress_last_len + '\r')
    sys.stderr.write(msg + '\r')
    sys.stderr.flush()
    progress_last_len = len(msg)
