#!/usr/bin/env python3
import sys

from logging import log, progress
from problem import Answer, Problem, Request, Endpoint, Cache


class Solver(object):
    def __init__(self, problem):
        self.problem = problem

    def sort_requests(self):
        self.problem.requests.sort(
            key=lambda request: request.Rn/(self.problem.videos[request.Rv]**0.5), reverse=True
        )

    def merge_requests(self):
        Rv_Re_to_Rn = {}
        for request in self.problem.requests:
            Rv_Re_to_Rn.setdefault((request.Rv, request.Re), 0)
            Rv_Re_to_Rn[(request.Rv, request.Re)] += request.Rn
        self.problem.requests = [Request(Rv, Re, Rn) for (Rv, Re), Rn in Rv_Re_to_Rn.items()]

    def solve(self):
        answer = Answer()

        self.merge_requests()
        self.sort_requests()

        for request in self.problem.requests:
            current_endpoint = self.problem.endpoints[request.Re]
            current_video = self.problem.videos[request.Rv]
            possible_caches = current_endpoint.caches
            possible_caches.sort(key=lambda cache: cache.Lc)

            already_stored = False
            for possible_cache in possible_caches:
                if request.Rv in possible_cache.store:
                    already_stored = True
            if already_stored:
                continue

            for possible_cache in possible_caches:
                answer_cache = answer.caches.get(possible_cache.c)
                if answer_cache:
                    if request.Rv in answer_cache.store:
                        break
                    elif answer_cache.X >= current_video:
                        answer.N += 1
                        answer_cache.store.append(request.Rv)
                        answer_cache.X -= current_video
                        break
                else:
                    answer.N += 1
                    possible_cache.X -= current_video
                    possible_cache.store.append(request.Rv)
                    answer.caches[possible_cache.c] = possible_cache
                    break

        return answer


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Usage: solve <problem>')
        sys.exit(-1)

    with open(sys.argv[1]) as f:
        problem = Problem(f)

    # Computing
    solver = Solver(problem)
    answer = solver.solve()

    # Output
    answer.print()
