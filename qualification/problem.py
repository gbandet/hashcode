from operator import itemgetter
from logging import log

class Problem(object):
    V, E, R, C, X = 0, 0, 0, 0, 0

    videos = [] # Sizes
    endpoints = []
    requests = []

    def __init__(self, file=None):
        if file is not None:
            self.V, self.E, self.R, self.C, self.X = [int(x) for x in file.readline().split()]
            log('V, E, R, C, X = %d, %d, %d, %d, %d' % (self.V, self.E, self.R, self.C, self.X))

            self.videos = [int(x) for x in file.readline().split()]
            log('Sum videos size: %d' % sum(self.videos))

            self.endpoints = []
            for i in range(self.E):
                Ld, K = [int(x) for x in file.readline().split()]
                endpoint = Endpoint(Ld, K, [])
                for j in range(K):
                    c, Lc = [int(x) for x in file.readline().split()]
                    cache = Cache(c, Lc, self.X, [])
                    endpoint.caches.append(cache)
                self.endpoints.append(endpoint)

            self.requests = []
            for i in range(self.R):
                Rv, Re, Rn = [int(x) for x in file.readline().split()]
                self.requests.append(Request(Rv, Re, Rn))

    def sort_requests(self):
        self.requests.sort(key=lambda x: x[2]/self.videos[x[0]], reverse=True)


class Request(object):
    Rv, Re, Rn = 0, 0, 0

    def __init__(self, Rv, Re, Rn):
        self.Rv = Rv
        self.Re = Re
        self.Rn = Rn


class Endpoint(object):
    Ld = 0
    K = 0
    caches = []

    def __init__(self, Ld, K, caches):
        self.Ld = Ld
        self.K = K
        self.caches = caches


class Cache(object):
    c = 0
    Lc = 0
    X = 0
    store = [] # vid

    def __init__(self, c, Lc, X, store):
        self.c = c
        self.Lc = Lc
        self.X = X
        self.store = store


class Answer(object):
    score = 0

    N = 0
    caches = {} # {cid: ([vids], remaining_size)}

    def __init__(self, file=None):
        if file is not None:
            self.N = int(file.readline())
            self.caches= {}
            for i in range(self.N):
                parts = file.readline().split()
                self.caches[int(parts[0])] = ([int(v) for v in parts[1:]], None)

    def print(self):
        print(len(self.caches))
        for cache in self.caches.values():
            print(' '.join(map(str, [cache.c] + cache.store)))
