#!/usr/bin/env python3
import sys

from logging import log, progress
from problem import Answer, Problem


class Optimizer(object):
    def __init__(self, problem, answer):
        self.problem = problem
        self.answer = answer

    def solve(self):
        caches_by_v = {}
        cache_sizes = {}
        for cid, (vids, _) in self.answer.caches.items():
            size = 0
            for vid in vids:
                caches = caches_by_v.setdefault(vid, [])
                caches.append(cid)
                size += self.problem.videos[vid]
            cache_sizes[cid] = size

        reqs_v = {}
        for Rv, requests_e in self.problem.req_by_e.items():
            reqs_e = reqs_v.setdefault(Rv, {})
            caches = caches_by_v.get(Rv, [])
            for Re, Rn in requests_e.items():
                best_c = -1
                best_gain = 0
                for cache in caches:
                    if (Re, cache) in self.problem.gains:
                        gain =  self.problem.gains[(Re, cache)]
                        if gain > best_gain:
                            best_c = cache
                            best_gain = gain
                reqs_e[Re] = (Rn, best_c, best_gain)

        videos = []
        for vid, reqs_e in reqs_v.items():
            score = 0
            for eid, req in reqs_e.items():
                score += req[0] * req[2]
            videos.append((vid, score, self.problem.videos[vid]))
        videos.sort(key=lambda x: x[1])

        for (vid, score, size) in videos:
            caches_scores = {}
            for eid, (Rn, cid, gain) in reqs_v[vid].items():
                caches_scores.setdefault(cid, 0)
                caches_scores[cid] += Rn * gain
            #caches_scores = list(caches_scores.items())
            #caches_scores.sort(key=lambda x: x[1])
            #worst_cache = caches_scores[0][0]
            #worst_score = caches_scores[0][1]

            for worst_cache, worst_score in caches_scores.items():

                updated = []
                updated_score = 0
                for eid, (Rn, cid, gain) in reqs_v[vid].items():
                    if cid != worst_cache:
                        updated_score += gain * Rn
                    else:
                        updated.append(eid)

                if worst_cache == -1:
                    continue

                for new_vid in range(self.problem.V):
                    if new_vid == vid:
                        continue
                    if self.problem.videos[new_vid] > self.problem.X - cache_sizes[worst_cache] + self.problem.videos[vid]:
                        continue

                    new_score = 0
                    changed = []
                    for eid, (Rn, cid, gain) in reqs_v[new_vid].items():
                        if (eid, worst_cache) not in self.problem.gains:
                            continue
                        new_gain = self.problem.gains[(eid, worst_cache)]
                        if new_gain > gain:
                            new_score += new_gain * Rn
                            changed.append((eid, new_gain))
                    if new_score > (worst_score - updated_score):
                        print ('improve: %d' % (new_score - (worst_score - updated_score)))
                        (vids, _) = self.answer.caches[worst_cache]
                        vids.remove(vid)
                        vids.append(new_vid)
                        self.answer.caches[worst_cache] = (vids, None)
                        # TODO size

                        print('best ?')
                        return self.answer

                        caches_by_v[vid].remove(worst_cache)
                        caches_by_v[new_vid].append(worst_cache)

    #                    for eid, new_gain in changes:
    #                        (Rn, cid, gain) = reqs_v[vid










        return self.answer


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('Usage: optimize <problem> <answer>')
        sys.exit(-1)

    with open(sys.argv[1]) as f:
        problem = Problem(f)
    with open(sys.argv[2]) as f:
        answer = Answer(f)

    # Computing
    solver = Optimizer(problem, answer)
    answer = solver.solve()

    # Output
    log('Score: %d' % answer.score)
    answer.print()
