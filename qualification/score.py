#!/usr/bin/env python3
import math
import sys

from problem import Answer, Problem


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('Usage: score <problem> <answer>')
        sys.exit(-1)

    with open(sys.argv[1]) as f:
        problem = Problem(f)
    with open(sys.argv[2]) as f:
        answer = Answer(f)

    score = 0
    sum_req = 0

    caches_by_v = {}
    for cid, (vids, _) in answer.caches.items():
        for vid in vids:
            caches = caches_by_v.setdefault(vid, [])
            caches.append(cid)

    for (Rv, Re, Rn) in problem.requests:
        sum_req += Rn
        Ld, caches_stats = problem.endpoints[Re]
        best_cache = -1
        best_lag = Ld
        if Rv in caches_by_v:
            for cache in caches_by_v[Rv]:
                for (c, Lc) in caches_stats:
                    if c == cache:
                        if Lc < best_lag:
                            best_lag = Lc
                            best_cache = c

        score += Rn * (Ld - best_lag)

    score = score * 1000
    score = math.floor(score / sum_req)
    print('Score: %d' % score)
